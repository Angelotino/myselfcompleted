//
//  AbstractTableViewController.swift
//  Myself
//
//  Created by Angelo on 25.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class AbstractTableViewController: UITableViewController, InitializeProtocol {

    convenience init() {
        self.init(style: .Grouped)

        initializeElementsAndApplyConstraints()
    }

    //MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        initializeElementsAndApplyConstraints()
        setupData()
    }

    //MARK: <Initialize protocol>
    func initializeElements() {}

    func customSetup() {}

    func endEditing() {}

    func setupConstraints() {}

    func setupData() {}

    func setupViews() {
        view.backgroundColor = Palette.White.color
    }
}
