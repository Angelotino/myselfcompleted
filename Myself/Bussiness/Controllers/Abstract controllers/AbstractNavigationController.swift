//
//  NavigationController.swift
//  Myself
//
//  Created by Angelo on 18.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class AbstractNavigationController: UINavigationController, InitializeProtocol {

    //MARK: - Lifecycle
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        setupData()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        initializeElementsAndApplyConstraints()
    }

    //MARK: <Initialize protocol>
    func initializeElements() {}

    func customSetup() {}

    func endEditing() {}

    func setupConstraints() {}

    func setupData() {}

    func setupViews() {
        navigationBar.shadowImage = UIImage.emptyImage
        navigationBar.barStyle = UIBarStyle.BlackOpaque
        navigationBar.tintColor = Palette.White.color
        navigationBar.barTintColor = Palette.CLightBlue.color
        navigationBar.titleTextAttributes = [NSForegroundColorAttributeName:Palette.White.color]
        navigationBar.translucent = true
    }
}
