//
//  InitializeProtocol.swift
//  Myself
//
//  Created by Angelo on 18.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

protocol InitializeProtocol {
    func customSetup()
    func initializeElements()
    func setupConstraints()
    func setupViews()
}

extension InitializeProtocol {
    internal func initializeElementsAndApplyConstraints() {
        defer {
            setupConstraints()
            customSetup()
        }
        initializeElements()
        setupViews()
    }
}
