//
//  LeftViewController.swift
//  Myself
//
//  Created by Angelo on 12.05.16.
//  Copyright © 2016 Angelo. All rights reserve
//

import UIKit
import SnapKit

class LeftViewController: AbstractViewController, UITableViewDelegate, UITableViewDataSource {

    //MARK: - Properties
    //MARK: -- private
    private let mainImageView = ImageView()

    private let tableView = UITableView(frame: CGRectZero, style: .Grouped)

    private static let cellItem: [LeftMenuItem] = [
        LeftMenuItem(vc: PrimeViewController.self, image: UIImage(image: .Apple)!, name: StringProvider.Prime.Programming.localizedString),
        LeftMenuItem(vc: ProjectsTableViewController.self, image: UIImage(image: .Projects)!, name: StringProvider.Prime.Projects.localizedString),
        LeftMenuItem(vc: AboutMeViewController.self, image: UIImage(image: .AboutMe)!, name: StringProvider.Prime.AboutMe.localizedString),
    ]

    //MARK: - Lifecycle
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)

        if let gesture = view.gestureRecognizers?.first {
            view.removeGestureRecognizer(gesture)
        }
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(endEditing)))
    }

    //MARK: - Methods
    //MARM: -- initialize methods
    internal override func initializeElements() {
        super.initializeElements()

        mainImageView.contentMode = UIViewContentMode.ScaleAspectFit
        mainImageView.image = UIImage(image: .AppleBg)

        tableView.backgroundColor = Palette.White.color
        tableView.contentInset = UIEdgeInsetsMake(-36, 0, 0, 0)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerClass(LeftMenuTableViewCell.self, forCellReuseIdentifier: LeftMenuTableViewCell.identifier)
        tableView.scrollEnabled = false

        for element in [mainImageView, tableView] {
            view.addSubview(element)
        }
    }

    //MARK: -- setup methods
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return .LightContent
    }

    internal override func setupConstraints() {
        super.setupConstraints()

        mainImageView.snp_makeConstraints { make in
            make.top.equalTo(view).offset(-20)
            make.leading.equalTo(view)
            make.trailing.equalTo(view)
            make.height.equalTo(200)
        }

        tableView.snp_makeConstraints { (make) in
            make.leading.equalTo(view)
            make.trailing.equalTo(view)
            make.top.equalTo(mainImageView.snp_bottom)
            make.bottom.equalTo(view)
        }
    }

    //MARK: <UITableViewDelegate, UITableViewDataSource>
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row < LeftViewController.cellItem.count {
            if let controller = LeftViewController.cellItem[indexPath.row].viewController?.init() {
                controller.title = LeftViewController.cellItem[indexPath.row].itemName
                mm_drawerController.setCenterViewController(NavigationViewController(rootViewController: controller), withCloseAnimation: true, completion: nil)
            }
        }
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(LeftMenuTableViewCell.identifier, forIndexPath: indexPath) as? LeftMenuTableViewCell ?? LeftMenuTableViewCell(identifier: LeftMenuTableViewCell.identifier)
        if indexPath.row < LeftViewController.cellItem.count {
            cell.textForCell = LeftViewController.cellItem[indexPath.row].itemName
            cell.imageForCell = LeftViewController.cellItem[indexPath.row].itemImage
        }

        return cell
    }

    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LeftViewController.cellItem.count
    }
}
