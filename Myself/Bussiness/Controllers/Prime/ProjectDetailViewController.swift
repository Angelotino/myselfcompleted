//
//  ProjectDetailViewController.swift
//  Myself
//
//  Created by Angelo on 29.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit
import Kingfisher

class ProjectDetailViewController: AbstractViewController {

    //MARK: - Properties
    //MARK: -- private properties
    private let languageBox = UIView()
    private let scrollView = UIScrollView()

    private let mainImageView = ImageView()

    private let descriptionLabel = Label()
    private let languageLabel = Label()
    private let titleLabel = Label()

    //MARK: -- public properties
    var projectDetail: ProjectObject?

    //MARK: - Lifecycle
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)

        mm_drawerController.openDrawerGestureModeMask =  MMOpenDrawerGestureMode.All
        navigationController?.navigationBar.backgroundImageWithAlpha(Palette.CLightBlue.color, alpha: Configuration.navigationBarAlpha)
    }

    //MARK: - Methods
    //MARK: -- initialize method
    internal override func initializeElements() {
        super.initializeElements()

        descriptionLabel.font = UIFont.systemFontOfSize(13)
        descriptionLabel.textColor = Palette.CLightRed.color

        languageBox.backgroundColor = Palette.CLightBlue.color

        languageLabel.textColor = Palette.White.color
        languageLabel.font = UIFont.systemFontOfSize(12)

        mainImageView.contentMode = .ScaleAspectFill
        mainImageView.clipsToBounds = true

        if let imageStringUrl = projectDetail?.image, url = NSURL(string: imageStringUrl) {
            mainImageView.kf_setImageWithURL(url)
        }

        if let detail = projectDetail {
            descriptionLabel.text = detail.description.applyNewLine
            languageLabel.text = detail.language
            titleLabel.text = detail.name
        }

        languageBox.addSubview(languageLabel)
        view.addSubview(scrollView)

        for element in [mainImageView, titleLabel, languageBox, descriptionLabel] {
            scrollView.addSubview(element)
        }
    }

    //MARK: -- setup methods
    internal override func setupViews() {
        super.setupViews()

        view.backgroundColor = Palette.White.color

        if let projectName = projectDetail?.name {
            title = projectName
        }
    }

    //MARK: -- action methods
    func openLeftMenuButtonPressed(sender: AnyObject) {
        self.mm_drawerController.openDrawerSide(.Left, animated: true, completion: nil)
    }

    //MARK: - Constraints
    internal override func setupConstraints() {
        super.setupConstraints()

        descriptionLabel.snp_makeConstraints { make in
            make.leading.equalTo(scrollView).inset(10)
            make.trailing.equalTo(scrollView).inset(10)
            make.top.equalTo(titleLabel.snp_bottom).offset(10)
            make.bottom.equalTo(scrollView)
        }

        languageBox.snp_makeConstraints { make in
            make.height.equalTo(35)
            make.leading.equalTo(scrollView)
            make.trailing.equalTo(scrollView)
            make.top.equalTo(mainImageView.snp_bottom).inset(30)
        }

        languageLabel.snp_makeConstraints { make in
            make.leading.equalTo(languageBox).inset(10)
            make.centerY.equalTo(languageBox)
            make.trailing.equalTo(languageBox).inset(10)
        }

        mainImageView.snp_makeConstraints { make in
            make.leading.equalTo(scrollView)
            make.trailing.equalTo(scrollView)
            make.centerX.equalTo(scrollView)
            make.top.equalTo(topLayoutGuide)
        }

        scrollView.snp_makeConstraints { make in
            make.edges.equalTo(view)
        }

        titleLabel.snp_makeConstraints { make in
            make.leading.equalTo(scrollView).inset(10)
            make.trailing.equalTo(scrollView).inset(10)
            make.top.equalTo(languageBox.snp_bottom).offset(10)
            make.top.equalTo(scrollView.snp_top).inset(150)
        }
    }
}
