//
//  ApiErrorChecker.swift
//  Myself
//
//  Created by Angelo on 22.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import Crashlytics

public enum InfoLogLevel: Int, CustomStringConvertible {
    case Verbose = 0
    case Info = 1
    case Debug = 2
    case Warning = 3
    case Error = 4

    public var description: String {
        switch self {
        case .Verbose:
            return "VERBOSE"
        case .Debug:
            return "DEBUG"
        case .Info:
            return "INFO"
        case .Warning:
            return "WARNING"
        case .Error:
            return "ERROR"
        }
    }
}

public func >=(a: InfoLogLevel, b: InfoLogLevel) -> Bool {
    return a.rawValue >= b.rawValue
}

public class InfoLogger {
    private static var instance = InfoLogger()

    class var defaultLogger: InfoLogger {
        return instance
    }
    var loggerIdentificationString: String = Configuration.appName
    var loggingDebugLevel: InfoLogLevel

    private init() {
        loggingDebugLevel = .Warning
    }

    public func verbose(message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .Verbose, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func debug(message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .Debug, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func info(message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .Info, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func warning(message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .Warning, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func error(message: String, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        log(message, logLevel: .Error, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
    }

    public func log(message: String, logLevel: InfoLogLevel = .Debug, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) {
        let logEntry = generateLogEntry(message, logLevel: logLevel, functionName: functionName, fileName: fileName, lineNumber: lineNumber)
        CLSLogv("%@", getVaList([logEntry]))
        if logLevel >= loggingDebugLevel {
            runOnTheMainThread {
                print(logEntry)
            }
        }
    }

    private func generateLogEntry(message: String, logLevel: InfoLogLevel = .Debug, functionName: String = #function, fileName: String = #file, lineNumber: Int = #line) -> String {
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.dateFormat = "HH:mm:ss yyyy/MM/dd"
        let dateString = formatter.stringFromDate(date)
        var threadString: String
        if NSThread.isMainThread() {
            threadString = "main"
        }
        else {
            if let threadName = NSThread.currentThread().name where !threadName.isEmpty {
                threadString = threadName
            }
            else {
                threadString = String(format:"%p", NSThread.currentThread())
            }
        }
        let fileNameOnly = (fileName as NSString).lastPathComponent
        return "\(logLevel.description) \(dateString) \(self.loggerIdentificationString)[\(threadString):\(fileNameOnly):\(functionName):line \(lineNumber)]: \(message)"
    }
}
