//
//  Dataservice+Extension.swift
//  Myself
//
//  Created by Angelo on 22.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

extension Dataservice {

    func getInfoAboutMe(completionHandler: ((success: Bool, object: AboutMeList?) -> ())) {
        let commander = NetworkCommander<AboutMeList>(urlManager: .AboutMe)
        commander.execute(completionHandler)
    }

    func getProjects(completionHandler: ((success: Bool, object: ProjectList?) -> ())) {
        let commander = NetworkCommander<ProjectList>(urlManager: .ProjectList)
        commander.execute(completionHandler)
    }
}
