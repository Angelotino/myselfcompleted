//
//  NetworkCommander.swift
//  Myself
//
//  Created by Angelo on 22.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Alamofire
import Alamofire_SwiftyJSON
import Foundation
import SwiftyJSON

protocol JSONObject {
    init?(json: JSON)
}

class NetworkCommander<T: JSONObject> {

    var urlManager: URLManager

    init(urlManager: URLManager) {
        self.urlManager = urlManager
    }

    typealias CompletionHandler = ((success: Bool, object: T?) -> ())

    func execute(completionHandler: CompletionHandler) {
        Alamofire.request(urlManager.method, urlManager.url, parameters: urlManager.parameters).responseSwiftyJSON(options: .AllowFragments) { (request, response, json, error) in
            if let statusCode = response?.statusCode {
                switch ApiErrorChecker(statusCode: statusCode) {
                case .Success:
                    completionHandler(success: true, object: T(json: json))
                case let .Error(error):
                    InfoLogger.defaultLogger.error(error)
                    completionHandler(success: false, object: nil)
                }
            }
            else {
                completionHandler(success: false, object: nil)
                InfoLogger.defaultLogger.error("Unknown error")
            }
        }
    }
}
