//
//  Color.swift
//  Myself
//
//  Created by Angelo on 11.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

    public convenience init(rgba: UInt32) {
        self.init(
            red: CGFloat((rgba & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgba & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgba & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }

    class func imageWithColor(color:UIColor,size:CGSize = CGSize(width: 1, height: 1)) -> UIImage {
        let rect = CGRectMake(0, 0, size.width, size.height)

        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return image
    }
}
