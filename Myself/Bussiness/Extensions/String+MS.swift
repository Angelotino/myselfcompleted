//
//  String.swift
//  Myself
//
//  Created by Angelo on 10.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

extension String {

    enum Table: String {
        case Prime = "Prime"
    }

    func localizedFromTable(table: Table) -> String {
        return NSLocalizedString(self, tableName: table.rawValue, comment: "")
    }

    var applyNewLine: String {
        return self.stringByReplacingOccurrencesOfString("NL", withString: "\n")
    }

    static var empty: String {
        return ""
    }
}
