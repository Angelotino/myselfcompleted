//
//  UITableView+MS.swift
//  Myself
//
//  Created by Angelo on 07.06.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

extension UITableView {

    func reloadSection<T: RawRepresentable where T.RawValue == Int>(index index: T, animation: UITableViewRowAnimation = .Fade) {
        beginUpdates()
        reloadSections(NSIndexSet(index: index.rawValue), withRowAnimation: animation)
        endUpdates()
    }
}
