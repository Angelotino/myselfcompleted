//
//  URLManager.swift
//  Myself
//
//  Created by Angelo on 24.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import Alamofire

protocol URLManagerProtocol {
    var parameters: [String: AnyObject] { get }
    var url: String { get }
    var method: Alamofire.Method { get }
}

enum URLManager: URLManagerProtocol {
    case AboutMe
    case ProjectList

    var parameters: [String: AnyObject] {
        switch self {
        case .AboutMe:
            fallthrough
        case .ProjectList:
            return [:]
        }
    }

    var url: String {
        var toReturn = String.empty
        switch self {
        case .AboutMe:
            toReturn = "mes.json"
        case .ProjectList:
            toReturn = "projs.json"
        }
        return Configuration.basicUrl+toReturn
    }

    var method: Alamofire.Method {
        switch self {
        case .AboutMe:
            fallthrough
        case .ProjectList:
            return Alamofire.Method.GET
        }
    }
}
