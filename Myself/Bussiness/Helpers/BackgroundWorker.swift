//
//  BackgroundWorker.swift
//  Myself
//
//  Created by Angelo on 02.06.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

private enum Queues {

    case Main
    case Background

    func queue() -> dispatch_queue_t {
        switch self {
        case .Main:
            return dispatch_get_main_queue()
        case .Background:
            return dispatch_get_global_queue(QOS_CLASS_BACKGROUND, 0)
        }
    }
}

func synced(lock: AnyObject, syncedClosure: dispatch_block_t) {
    objc_sync_enter(lock)
    syncedClosure()
    objc_sync_exit(lock)
}

public func runOnTheMainThread(sync: Bool = false, mainClosure: dispatch_block_t) {
    if NSThread.currentThread().isMainThread {
        mainClosure()
    }
    else {
        if sync {
            dispatch_sync(Queues.Main.queue(), mainClosure)
        }
        else {
            dispatch_async(Queues.Main.queue(), mainClosure)
        }
    }
}
