//
//  ProjectList.swift
//  Myself
//
//  Created by Angelo on 29.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ProjectList: JSONObject {

    var projects: [ProjectObject] = []

    init?(json: JSON) {
        if let json = json.array {
            for project in json {
                if let project = ProjectObject(json: project) {
                    projects.append(project)
                }
            }
        }
    }
}
