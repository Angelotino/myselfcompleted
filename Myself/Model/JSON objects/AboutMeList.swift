//
//  AboutMeList.swift
//  Myself
//
//  Created by Angelo on 08.06.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import SwiftyJSON

struct AboutMeList: JSONObject {

    var aboutMeObject: [AboutMeObject] = []

    init?(json: JSON) {
        if let json = json.array {
            for json in json {
                if let object = AboutMeObject(json: json) {
                    aboutMeObject.append(object)
                }
            }
        }
    }
}
