//
//  Me.swift
//  Myself
//
//  Created by Angelo on 29.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation
import CoreData

enum CoreDataErrors: ErrorType {
    case UnableFetchData
    case EnableInsertRequest
    case ZeroResult
}

class Me: NSManagedObject {

    static var object: Me?

    func updateWithObject(object: AboutMeList) {
        if let object = object.aboutMeObject.first {
            mAge = object.age
            mCity = object.city
            mDescription = object.description
            mEmail = object.email
            mId = object.id
            mName = object.name
            mPhoneNumber = object.phoneNumber
        }
    }

    class func newFromObject(object: AboutMeList) throws -> Me {
        let context = Dataservice.sharedInstance.context

        defer {
            do {
                try context.save()
            }
            catch {
                CoreDataErrors.EnableInsertRequest
            }
        }

        let request = NSFetchRequest(entityName: Configuration.cdMe)
        request.predicate = NSPredicate(format: "mId = %d", object.aboutMeObject.first?.id ?? 0)

        do {
            let result = try context.executeFetchRequest(request)
            if let result = result as? [Me], let toReturn = result.first {
                toReturn.updateWithObject(object)
                return toReturn
            } else {
                let toReturn = NSEntityDescription.insertNewObjectForEntityForName(Configuration.cdMe, inManagedObjectContext: context) as? Me ?? Me()
                toReturn.updateWithObject(object)
                return toReturn
            }
        }
        catch {
            throw CoreDataErrors.EnableInsertRequest
        }
    }

    class func fetchData() throws {
        let context = Dataservice.sharedInstance.context
        let request = NSFetchRequest(entityName: Configuration.cdMe)

        do {
            let result = try context.executeFetchRequest(request)
            if let result = result as? [Me] where result.count > 0 {
                object = result.first
            }
        }
        catch {
            throw CoreDataErrors.UnableFetchData
        }
    }
}
