//
//  DataItem.swift
//  Myself
//
//  Created by Angelo on 25.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import Foundation

struct LeftMenuItem {

    var itemImage: UIImage
    var itemName: String
    var viewController: UIViewController.Type?

    init(vc: UIViewController.Type?, image: UIImage, name: String) {
        itemImage = image
        itemName = name
        viewController = vc
    }
}
