//
//  Label.swift
//  Myself
//
//  Created by Angelo on 20.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class Label: UILabel, InitializeProtocol {

    override init(frame: CGRect) {
        super.init(frame: frame)

        initializeElementsAndApplyConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initializeElementsAndApplyConstraints()
    }

    //MARK: <Initialize protocol>
    func customSetup() {}

    func initializeElements() {
        numberOfLines = 0
    }

    func setupConstraints() {}

    func setupViews() {}
}
