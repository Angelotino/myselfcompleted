//
//  TextField.swift
//  Myself
//
//  Created by Angelo on 20.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class TextField: UITextField, InitializeProtocol {

    override init(frame: CGRect) {
        super.init(frame: frame)

        initializeElementsAndApplyConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initializeElementsAndApplyConstraints()
    }

    //MARK: <Initialize protocol>
    func customSetup() {}

    func initializeElements() {}

    func setupConstraints() {}

    func setupViews() {}
}
