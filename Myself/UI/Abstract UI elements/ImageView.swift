//
//  ImageView.swift
//  Myself
//
//  Created by Angelo on 20.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class ImageView: UIImageView, InitializeProtocol {

    convenience init() {
        self.init(frame: CGRectZero)

        initializeElementsAndApplyConstraints()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        initializeElementsAndApplyConstraints()
    }

    override init(image: UIImage?) {
        super.init(image: image)

        initializeElementsAndApplyConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initializeElementsAndApplyConstraints()
    }

    //MARK: <Initialize protocol>
    func customSetup() {}

    func initializeElements() {}

    func setupConstraints() {}

    func setupViews() {}
}
