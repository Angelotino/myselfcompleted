//
//  LeftMenuTableViewCell.swift
//  Myself
//
//  Created by Angelo on 25.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class LeftMenuTableViewCell: TableViewCell {

    //MARK: - Properties
    //MARK: -- private properties
    private let cellText = Label()

    private let leftImageView = ImageView()

    //MARK: -- public properties
    var imageForCell: UIImage? {
        get {
            return leftImageView.image
        }
        set {
            leftImageView.image = newValue
        }
    }

    var textForCell: String? {
        get {
            return cellText.text
        }
        set {
            cellText.text = newValue
        }
    }

    //MARK: - Methods
    //MARK: -- initialize methods
    internal override func initializeElements() {
        super.initializeElements()

        for element in [leftImageView, cellText] {
            contentView.addSubview(element)
        }
    }

    //MARK: -- setup methods
    internal override func setupConstraints() {
        super.setupConstraints()

        cellText.snp_makeConstraints { make in
            make.centerY.equalTo(contentView)
            make.trailing.equalTo(contentView)
            make.leading.equalTo(leftImageView.snp_trailing).offset(10)
        }

        leftImageView.snp_makeConstraints { make in
            make.centerY.equalTo(contentView)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.leading.equalTo(contentView).inset(5)
        }
    }

    //MARK: -- setup methods
    internal override func customSetup() {
        super.customSetup()

        backgroundColor = Palette.White.color
        selectionStyle = .None
        separatorInset = UIEdgeInsetsZero
    }
}
