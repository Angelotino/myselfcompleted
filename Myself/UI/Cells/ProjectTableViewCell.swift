//
//  ProjectTableViewCell.swift
//  Myself
//
//  Created by Angelo on 28.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit
import Kingfisher

class ProjectTableViewCell:  TableViewCell {

    //MARK: - Properties
    //MARK: -- private properties
    private let titleText = Label()
    private let detailText = Label()

    private let leftImageView = ImageView()

    //MARK: -- public properties
    var detail: String? {
        get {
            return detailText.text
        }
        set {
            detailText.text = newValue
        }
    }

    var imageURLString: String? {
        didSet {
            if let imageURL = imageURLString {
                if let url = NSURL(string: imageURL) {
                    leftImageView.kf_setImageWithURL(url)
                }
            }
        }
    }

    var title: String? {
        get {
            return titleText.text
        }
        set {
            titleText.text = newValue
        }
    }

    //MARK: - Methods
    //MARK: -- initialize methods
    internal override func initializeElements() {
        super.initializeElements()

        detailText.textColor = Palette.Gray.color
        detailText.font = UIFont.systemFontOfSize(13)

        leftImageView.layer.cornerRadius = Configuration.cellCornerRadius
        leftImageView.clipsToBounds = true

        for element in [leftImageView, titleText, detailText] {
            contentView.addSubview(element)
        }
    }

    //MARK: -- setup methods
    internal override func setupConstraints() {
        super.setupConstraints()

        detailText.snp_makeConstraints { make in
            make.top.equalTo(titleText.snp_bottom)
            make.leading.equalTo(contentView).inset(55)
            make.trailing.equalTo(contentView).inset(10)
            make.bottom.equalTo(contentView).inset(2)
        }

        leftImageView.snp_makeConstraints { make in
            make.top.equalTo(contentView).inset(5)
            make.width.equalTo(40)
            make.height.equalTo(40)
            make.leading.equalTo(contentView).inset(5)
        }

        titleText.snp_makeConstraints { make in
            make.height.equalTo(25)
            make.top.equalTo(contentView).inset(5)
            make.trailing.equalTo(contentView)
            make.leading.equalTo(leftImageView.snp_trailing).offset(10)
        }
    }

    //MARK: -- setup methods
    internal override func customSetup() {
        super.customSetup()

        backgroundColor = Palette.White.color
        selectionStyle = .None
        separatorInset = UIEdgeInsetsZero
    }
}
