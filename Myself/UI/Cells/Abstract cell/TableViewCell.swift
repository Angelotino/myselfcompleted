//
//  TableViewCell.swift
//  Myself
//
//  Created by Angelo on 25.05.16.
//  Copyright © 2016 Angelo. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell, InitializeProtocol {

    convenience init(identifier: String) {
        self.init(style: .Default, reuseIdentifier: identifier)

        initializeElementsAndApplyConstraints()
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        initializeElementsAndApplyConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        initializeElementsAndApplyConstraints()
    }

    //MARK: <Initialize protocol>
    func customSetup() {}

    func initializeElements() {}

    func setupConstraints() {}

    func setupViews() {}
}
